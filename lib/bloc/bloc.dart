import 'package:monitoring_skripsi/model/mdl_dosen.dart';
import 'package:monitoring_skripsi/model/mdl_list_mahasiswa.dart';
import 'package:monitoring_skripsi/repository/admin_repo.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:rxdart/rxdart.dart';

class BlocListMhs {
  final _fethMhs = PublishSubject<List<MdlListMasiswa>>();
  final _repoShowMhs = DosenRepo();

  final _repoShowDosen = AdminRepo();
  final _fethDosen = PublishSubject<List<MdlDosen>>();

  final _repds = AdminRepo();
  final _nidn = BehaviorSubject<String>();
  final _nama = BehaviorSubject<String>();
  final _keahlian = BehaviorSubject<String>();
  final _nohp = BehaviorSubject<String>();
  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Function(String) get nidn => _nidn.sink.add;
  Function(String) get nama => _nama.sink.add;
  Function(String) get keahlian => _keahlian.sink.add;
  Function(String) get nohp => _nohp.sink.add;
  Function(String) get email => _email.sink.add;
  Function(String) get password => _password.sink.add;

  blocAddDosen() {
    _repds.repoAddDosen(_nidn.value, _nama.value, _keahlian.value, _nohp.value,
        _email.value, _password.value);
  }

  Stream<List<MdlListMasiswa>> get listMhs => _fethMhs.stream;

  fetcchAllMhs(String nip) async {
    List<MdlListMasiswa> listMhs = await _repoShowMhs.fetchAllMhs(nip);

    _fethMhs.add(listMhs);
    // print(listMhs.length.toString() + 'ini bloc');
  }

  Stream<List<MdlDosen>> get dataDosen => _fethDosen.stream;

  fetcchAllDosen() async {
    List<MdlDosen> listDosen = await _repoShowDosen.fetchAllDosen();

    _fethDosen.add(listDosen);
  }

  dispose() {
    _fethMhs.close();
    _fethDosen.close();
    _nidn.close();
    _nama.close();
    _keahlian.close();
    _nohp.close();
    _email.close();
    _password.close();
  }
}
