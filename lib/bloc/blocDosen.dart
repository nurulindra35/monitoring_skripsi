import 'package:monitoring_skripsi/model/mdl_list_mahasiswa.dart';

import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:rxdart/rxdart.dart';

class BlocDosen {
  final _repoShowMhs = DosenRepo();
  final _fethMhs = PublishSubject<List<MdlListMasiswa>>();

  Stream<List<MdlListMasiswa>> get listMhs => _fethMhs.stream;

  fetcchAllMhs(String nip) async {
    List<MdlListMasiswa> listMhs = await _repoShowMhs.fetchAllMhs(nip);

    _fethMhs.add(listMhs);
    print(listMhs.length.toString() + 'ini bloc');
  }

  dispose() {
    _fethMhs.close();
  }
}
