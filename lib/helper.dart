class HelperUrl {
  String apiRegister =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/register/';
  String apiLogin = 'https://monitoringtamobile.tatiumy.com/public/api/login';
  String apiAddProfil =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/add-profil';
  String apiGetAllDosen =
      'https://monitoringtamobile.tatiumy.com/public/api/admin/get-dosen/';
  String apiGeDetailProfil =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/profil-ta/';
  String apiGeMonnitoring =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/get-monitoring/';
  String apiAddMonnitoring =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/insert-monitoring/';
  String apiListDosbim =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/list-dosbim/';
  String apiAddPerbaikan =
      'https://monitoringtamobile.tatiumy.com/public/api/mahasiswa/update-monitoring/';
  String apiListMahasiswa =
      'https://monitoringtamobile.tatiumy.com/public/api/dosen/list-mhs/';
  String apiListMonitoringMhs =
      'https://monitoringtamobile.tatiumy.com/public/api/dosen/get-monitoring/';
  String apiKomentarDosen =
      'https://monitoringtamobile.tatiumy.com/public/api/dosen/update-komentar/';
  String apiStatusMonitoring =
      'https://monitoringtamobile.tatiumy.com/public/api/dosen/update-status/';
  String apivalidateMonitoring =
      'https://monitoringtamobile.tatiumy.com/public/api/dosen/validate-monitoring/';
  String apiAddDosen =
      'https://monitoringtamobile.tatiumy.com/public/api/admin/add-dosen/';

  //untuk mengambil data url
  String register() {
    return this.apiRegister;
  }

  String login() {
    return this.apiLogin;
  }

  String addProfil() {
    return this.apiAddProfil;
  }

  String getAllDosen() {
    return this.apiGetAllDosen;
  }

  String getDetailProfil() {
    return this.apiGeDetailProfil;
  }

  String getMonitoring() {
    return this.apiGeMonnitoring;
  }

  String addMonitoring() {
    return this.apiAddMonnitoring;
  }

  String lisDosbim() {
    return this.apiListDosbim;
  }

  String addPerbaikan() {
    return this.apiAddPerbaikan;
  }

  String listMahasiswa() {
    return this.apiListMahasiswa;
  }

  String listMonitoringMhs() {
    return this.apiListMonitoringMhs;
  }

  String komentarDosen() {
    return this.apiKomentarDosen;
  }

  String updateStatusMonitoring() {
    return this.apiStatusMonitoring;
  }

  String validateMonitoring() {
    return this.apivalidateMonitoring;
  }

  String addDosen() {
    return this.apiAddDosen;
  }
}
