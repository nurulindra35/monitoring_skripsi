import 'dart:convert';

MdlDetailProfil mdlDetailProfilFromMap(String str) =>
    MdlDetailProfil.fromMap(json.decode(str));

String mdlDetailProfilToMap(MdlDetailProfil data) => json.encode(data.toMap());

class MdlDetailProfil {
  MdlDetailProfil({
    this.idProfil,
    this.judul,
    this.dosenPembimbing1,
    this.dosenPembimbing2,
    this.tanggalMulai,
    this.tanggalSelesai,
    this.statusBimbingan,
  });
  int idProfil;
  String judul;
  String dosenPembimbing1;
  String dosenPembimbing2;
  String tanggalMulai;
  String tanggalSelesai;
  String statusBimbingan;

  MdlDetailProfil copyWith({
    int idProfil,
    String judul,
    String dosenPembimbing1,
    String dosenPembimbing2,
    String tanggalMulai,
    String tanggalSelesai,
    String statusBimbingan,
  }) =>
      MdlDetailProfil(
        idProfil: idProfil ?? this.idProfil,
        judul: judul ?? this.judul,
        dosenPembimbing1: dosenPembimbing1 ?? this.dosenPembimbing1,
        dosenPembimbing2: dosenPembimbing2 ?? this.dosenPembimbing2,
        tanggalMulai: tanggalMulai ?? this.tanggalMulai,
        tanggalSelesai: tanggalSelesai ?? this.tanggalSelesai,
        statusBimbingan: statusBimbingan ?? this.statusBimbingan,
      );

  factory MdlDetailProfil.fromMap(Map<String, dynamic> json) => MdlDetailProfil(
        idProfil: json["id_profil"] == null ? null : json["id_profil"],
        judul: json["judul"] == null ? null : json["judul"],
        dosenPembimbing1: json["Dosen Pembimbing 1"] == null
            ? null
            : json["Dosen Pembimbing 1"],
        dosenPembimbing2: json["Dosen Pembimbing 2"] == null
            ? null
            : json["Dosen Pembimbing 2"],
        tanggalMulai:
            json["tanggal_mulai"] == null ? null : json["tanggal_mulai"],
        tanggalSelesai:
            json["tanggal_selesai"] == null ? null : json["tanggal_selesai"],
        statusBimbingan:
            json["status_bimbingan"] == null ? null : json["status_bimbingan"],
      );

  Map<String, dynamic> toMap() => {
        "id_profil": idProfil == null ? null : idProfil,
        "judul": judul == null ? null : judul,
        "Dosen Pembimbing 1":
            dosenPembimbing1 == null ? null : dosenPembimbing1,
        "Dosen Pembimbing 2":
            dosenPembimbing2 == null ? null : dosenPembimbing2,
        "tanggal_mulai": tanggalMulai == null ? null : tanggalMulai,
        "tanggal_selesai": tanggalSelesai == null ? null : tanggalSelesai,
        "status_bimbingan": statusBimbingan == null ? null : statusBimbingan,
      };
}
