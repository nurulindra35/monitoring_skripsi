class MdlDosen {
  String nidn;
  String nama;
  String keahlian;
  String nohp;
  String password;
  String email;
  

  MdlDosen(
      {this.nidn,
      this.nama,
      this.keahlian,
      this.nohp,
      this.email,
      this.password});

  factory MdlDosen.fromMap(Map<String, dynamic> objec) => MdlDosen(
      nidn: objec['nidn'],
      nama: objec['nama'],
      keahlian: objec['keahlian'],
      nohp: objec['no_hp'],
      email: objec['email'],
      password: objec['password']);

  Map<String, dynamic> toMap() => {
        'nidn': nidn,
        'nama': nama,
        'keahlian': keahlian,
        'no_hp': nohp,
        'email': email,
        'password': password
      };
}
