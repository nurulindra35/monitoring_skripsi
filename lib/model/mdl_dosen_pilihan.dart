class MdlDosenPilihan {
  String nidn;
  String nama;

  MdlDosenPilihan({this.nama, this.nidn});

   factory MdlDosenPilihan.fromMap(Map<String, dynamic> objec) => MdlDosenPilihan(
      nidn: objec['nidn'],
      nama: objec['nama'],
     );
  Map<String, dynamic> toMap() => {
        'nidn': nidn,
        'nama': nama,
      };
}
