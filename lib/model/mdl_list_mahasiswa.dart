class MdlListMasiswa {
  String nama;
  String nim;
  String judul;
  String status;

  MdlListMasiswa({
    this.nama,
    this.nim,
    this.judul,
    this.status,
  });

  factory MdlListMasiswa.fromMap(Map<String, dynamic> objec) => MdlListMasiswa(
        nama: objec['nama'],
        nim: objec['nim'],
        judul: objec['judul'],
        status: objec['status_bimbingan'],
      );
  Map<String, dynamic> toMap() =>
      {'nim': nim, 'nama': nama, 'judul': judul, 'status_bimbingan': status};
}
