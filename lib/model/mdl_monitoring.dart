// To parse this JSON data, do
//
//     final mdlMonitoring = mdlMonitoringFromMap(jsonString);

import 'dart:convert';

MdlMonitoring mdlMonitoringFromMap(String str) =>
    MdlMonitoring.fromMap(json.decode(str));

String mdlMonitoringToMap(MdlMonitoring data) => json.encode(data.toMap());

class MdlMonitoring {
  MdlMonitoring({
    this.idMonitoring,
    this.idProfil,
    this.tangalBimbingan,
    this.materiBimbingan,
    this.namaDosen,
    this.statusMonitoring,
    this.komentar,
    this.hasilPerbaikan,
    this.halaman,
  });

  int idMonitoring;
  int idProfil;
  String tangalBimbingan;
  String materiBimbingan;
  String namaDosen;
  String statusMonitoring;
  String komentar;
  String hasilPerbaikan;
  String halaman;

  MdlMonitoring copyWith({
    int idMonitoring,
    int idProfil,
    String tangalBimbingan,
    String materiBimbingan,
    String namaDosen,
    String statusBimbingan,
    String komentar,
    String hasilPerbaikan,
    String halaman,
  }) =>
      MdlMonitoring(
        idMonitoring: idMonitoring ?? this.idMonitoring,
        idProfil: idProfil ?? this.idProfil,
        tangalBimbingan: tangalBimbingan ?? this.tangalBimbingan,
        materiBimbingan: materiBimbingan ?? this.materiBimbingan,
        namaDosen: namaDosen ?? this.namaDosen,
        statusMonitoring: statusBimbingan ?? this.statusMonitoring,
        komentar: komentar ?? this.komentar,
        hasilPerbaikan: hasilPerbaikan ?? this.hasilPerbaikan,
        halaman: halaman ?? this.halaman,
      );

  factory MdlMonitoring.fromMap(Map<String, dynamic> json) => MdlMonitoring(
        idMonitoring:
            json["id_monitoring"] == null ? null : json["id_monitoring"],
        idProfil: json["id_profil"] == null ? null : json["id_profil"],
        tangalBimbingan:
            json["tangal_bimbingan"] == null ? null : json["tangal_bimbingan"],
        materiBimbingan:
            json["materi_bimbingan"] == null ? null : json["materi_bimbingan"],
        namaDosen: json["nama_dosen"] == null ? null : json["nama_dosen"],
        statusMonitoring: json["status_monitoring"],
        komentar: json["komentar"] == null ? null : json["komentar"],
        hasilPerbaikan:
            json["hasil_perbaikan"] == null ? null : json["hasil_perbaikan"],
        halaman: json["halaman"] == null ? null : json["halaman"],
      );

  Map<String, dynamic> toMap() => {
        "id_monitoring": idMonitoring == null ? null : idMonitoring,
        "id_profil": idProfil == null ? null : idProfil,
        "tangal_bimbingan": tangalBimbingan == null ? null : tangalBimbingan,
        "materi_bimbingan": materiBimbingan == null ? null : materiBimbingan,
        "nama_dosen": namaDosen == null ? null : namaDosen,
        "status_monitoring": statusMonitoring,
        "komentar": komentar == null ? null : komentar,
        "hasil_perbaikan": hasilPerbaikan == null ? null : hasilPerbaikan,
        "halaman": halaman == null ? null : halaman,
      };
}
