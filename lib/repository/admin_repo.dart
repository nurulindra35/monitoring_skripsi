import 'package:monitoring_skripsi/model/mdl_dosen.dart';
import 'package:monitoring_skripsi/service/admin_service.dart';

class AdminRepo {
  final serv = AdminService();
  Future<List<MdlDosen>> fetchAllDosen() => serv.fetchDosen();

  Future repoAddDosen(String nidn, String nama, String keahlian, String nohp,
          String email, String password) async =>
      await serv.tambahDosen(nidn, nama, keahlian, nohp, email, password);
}
