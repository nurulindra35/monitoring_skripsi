import 'package:monitoring_skripsi/service/auth_service.dart';

class AuthRepo {
  final serviceAuth = AuthService();
  Future repoRegister(
    String nim,
    String nama,
    String email,
    String password,
    String nohp,
  ) async =>
      await serviceAuth.registerMahasiswa(nim, nama, email, password, nohp);

  Future repoLogin(String email, String password) =>
      serviceAuth.loginUser(email, password);
}
