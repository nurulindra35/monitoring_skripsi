import 'package:monitoring_skripsi/model/mdl_list_mahasiswa.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/service/dosen_service.dart';

class DosenRepo {
  final serv = DosenService();
  Future<List<MdlListMasiswa>> fetchAllMhs(String nip) => serv.fetchMhs(nip);

  Future<List<MdlMonitoring>> fetchMntr(String nip, String nim) =>
      serv.fetchMonitoring(nip, nim);

  Future komen(String komentar, int id) async {
    await serv.komentar(komentar, id);
  }

  Future updateStatus(String status, int id) async {
    await serv.status(status, id);
  }

  Future<List<MdlMonitoring>> validateMonitoring( String nim) =>
      serv.validateMntr( nim);
}
