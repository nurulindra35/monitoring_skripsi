import 'package:monitoring_skripsi/model/mdl_dosen_pilihan.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/service/monitoring_service.dart';

class RepoMonitoring {
  final serv = MonitoringService();
  Future<List<MdlMonitoring>> getMonitoring(String nim) =>
      serv.fetchMonitoring(nim);
  Future addMonitoring(String idBimbingan, String tanggal, String keterangan,
          String nip) async =>
      {await serv.addMonitoring(idBimbingan, tanggal, keterangan, nip)};
  Future<List<MdlDosenPilihan>> fetchDosenpilihan(String nim) =>
      serv.fetchDosenSelect(nim);

  Future repoEditMonitoring(
          String hasilPerbaikan, String halaman, String idMonitoring) async =>
      await serv.addPerbaikanPage(hasilPerbaikan, halaman, idMonitoring);
}
