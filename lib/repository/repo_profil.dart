import 'package:monitoring_skripsi/model/mdl_detail_profil.dart';
import 'package:monitoring_skripsi/service/profil_service.dart';

class RepoProfil {
  final servAdd = ProfilService();

  Future addProfil(
          String nim,
          String judul,
          String tanggalMulai,
          String tanggalSelesai,
          String dospem1,
          String dospem2,
          String status1,
          String status2) async =>
      {
        await servAdd.addProfil(nim, judul, tanggalMulai, tanggalSelesai,
            dospem1, dospem2, status1, status2)
      };
       Future<List<MdlDetailProfil>> fetchDetail(String nim) =>
      servAdd.fetchBimbingan(nim);
}
