import 'dart:convert';

import 'package:monitoring_skripsi/model/mdl_dosen.dart';
import 'package:http/http.dart' as http;

import '../helper.dart';

class AdminService {
  String url = HelperUrl().getAllDosen();
  String addDosen = HelperUrl().addDosen();

  Future<List<MdlDosen>> fetchDosen() async {
    var res = await http.get("$url");
    var dataDosen = jsonDecode(res.body);

    List<MdlDosen> listDosen = [];

    for (int i = 0; i < dataDosen.length; i++) {
      var modelDosen = MdlDosen(
          nidn: dataDosen[i]['nidn'],
          nama: dataDosen[i]['nama'],
          keahlian: dataDosen[i]['keahlian'],
          nohp: dataDosen[i]['no_hp'],
          email: dataDosen[i]['email'],
          password: dataDosen[i]['password']);

      listDosen.add(modelDosen);
    }
    return listDosen;
  }

  Future tambahDosen(String nidn, String nama, String keahlian, String nohp,
      String email, String password) async {
    var res = await http.post('$addDosen', body: {
      'nidn': nidn,
      'nama': nama,
      'keahlian': keahlian,
      'no_hp': nohp,
      'email': email,
      'password': password,
    });

    if (res.statusCode == 200) {
      print('sukses');
    }
    return json.decode(res.body);
  }
}
