import 'dart:convert';

import 'package:http/http.dart' as http;

import '../helper.dart';

class AuthService {
  String register = HelperUrl().register();
  String login = HelperUrl().login();
  Future registerMahasiswa(
      String nim, String nama, email, String password, String nohp) async {
    var result = await http.post('$register', body: {
      'nim': nim,
      'nama': nama,
      'email': email,
      'password': password,
      'no_hp': nohp
    });
    // if (result.statusCode == 200) {
    //   print('Add Sucses');
    // } else {
    //   print(result.body);
    // }
    return json.decode(result.body);
  }

  Future loginUser(String email, String password) async {
    Map<String, String> header = {"Content-type": "application/json"};
    var dataLogin = await http.post(
      '$login',
      body: {'email': email, 'password': password},
    );

    print(dataLogin.body);

    return json.decode(dataLogin.body);

    // var datauser = json.decode(dataLogin.body);
    // print(datauser);
  }
}
