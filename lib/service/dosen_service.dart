import 'dart:convert';

import 'package:monitoring_skripsi/helper.dart';
import 'package:monitoring_skripsi/model/mdl_list_mahasiswa.dart';
import 'package:http/http.dart' as http;
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';

class DosenService {
  String listMhsApi = HelperUrl().listMahasiswa();
  String listMonitoringMhs = HelperUrl().listMonitoringMhs();
  String komentarDosen = HelperUrl().komentarDosen();
  String validate = HelperUrl().updateStatusMonitoring();
  String validateMonitoring = HelperUrl().validateMonitoring();

  Future<List<MdlListMasiswa>> fetchMhs(String nip) async {
    var res = await http.get('$listMhsApi' + '$nip');
    var dataMhs = jsonDecode(res.body);

    List<MdlListMasiswa> listMhs = [];

    for (int i = 0; i < dataMhs.length; i++) {
      var modelMhs = MdlListMasiswa(
        nama: dataMhs[i]['nama'],
        nim: dataMhs[i]['nim'],
        judul: dataMhs[i]['judul'],
        status: dataMhs[i]['status_bimbingan'],
      );
      listMhs.add(modelMhs);
    }
    return listMhs;
  }

  Future<List<MdlMonitoring>> fetchMonitoring(String nip, String nim) async {
    var res = await http.get("$listMonitoringMhs" + '$nip' + '/' + '$nim');
    var dataMhs = jsonDecode(res.body);

    List<MdlMonitoring> listMhs = [];

    for (int i = 0; i < dataMhs.length; i++) {
      var modelMhs = MdlMonitoring(
          idMonitoring: dataMhs[i]['id_monitoring'],
          idProfil: dataMhs[i]['id_profil'],
          tangalBimbingan: dataMhs[i]['tangal_bimbingan'],
          materiBimbingan: dataMhs[i]['materi_bimbingan'],
          namaDosen: dataMhs[i]['nama_dosen'],
          komentar: dataMhs[i]['komentar'],
          hasilPerbaikan: dataMhs[i]['hasil_perbaikan'],
          halaman: dataMhs[i]['halaman'],
          statusMonitoring: dataMhs[i]['status_monitoring']);
      listMhs.add(modelMhs);
    }
    print(listMhs.length);
    return listMhs;
  }

  Future komentar(String komentar, int idMonitoring) async {
    var res = await http
        .post('$komentarDosen' + '$idMonitoring', body: {'komentar': komentar});

    if (res.statusCode == 200) {
      print('sukses');
    }
    return json.decode(res.body);
  }

  Future status(String status, int id) async {
    var res = await http
        .post('$validate' + '$id', body: {'status_monitoring': status});

    if (res.statusCode == 200) {
      print('sukses');
    }

    return json.decode(res.body);
  }

  Future<List<MdlMonitoring>> validateMntr(String nim) async {
    var res = await http.get("$validateMonitoring" + '$nim');
    var dataMhs = jsonDecode(res.body);

    List<MdlMonitoring> listMhs = [];

    for (int i = 0; i < dataMhs.length; i++) {
      var modelMhs = MdlMonitoring(
          idMonitoring: dataMhs[i]['id_monitoring'],
          idProfil: dataMhs[i]['id_profil'],
          tangalBimbingan: dataMhs[i]['tangal_bimbingan'],
          materiBimbingan: dataMhs[i]['materi_bimbingan'],
          namaDosen: dataMhs[i]['nama_dosen'],
          statusMonitoring: dataMhs[i]['status_monitoring']);
      listMhs.add(modelMhs);
    }
    print(listMhs.length);
    return listMhs;
  }
}
