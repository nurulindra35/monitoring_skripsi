import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:monitoring_skripsi/model/mdl_dosen_pilihan.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';

import '../helper.dart';

class MonitoringService {
  String getMonitring = HelperUrl().getMonitoring();
  String addMonitring = HelperUrl().addMonitoring();
  String listDosbim = HelperUrl().lisDosbim();
  String addPerbaikan = HelperUrl().addPerbaikan();

  Future<List<MdlMonitoring>> fetchMonitoring(String nim) async {
    var res = await http.get("$getMonitring" + "$nim");
    var dataMhs = jsonDecode(res.body);

    List<MdlMonitoring> listMhs = [];

    for (int i = 0; i < dataMhs.length; i++) {
      var modelMhs = MdlMonitoring(
          idMonitoring: dataMhs[i]['id_monitoring'],
          idProfil: dataMhs[i]['id_profil'],
          tangalBimbingan: dataMhs[i]['tangal_bimbingan'],
          materiBimbingan: dataMhs[i]['materi_bimbingan'],
          namaDosen: dataMhs[i]['nama_dosen'],
          komentar: dataMhs[i]['komentar'],
          hasilPerbaikan: dataMhs[i]['hasil_perbaikan'],
          halaman: dataMhs[i]['halaman'],
          statusMonitoring: dataMhs[i]['status_monitoring']);
      listMhs.add(modelMhs);
    }
    print(listMhs.length);
    return listMhs;
  }

  Future addMonitoring(String idProfil, String tanggalBimbingan,
      String materiBimbingan, String nidn) async {
    var res = await http.post('$addMonitring' + '$idProfil', body: {
      'tanggal_bimbingan': tanggalBimbingan,
      'materi_bimbingan': materiBimbingan,
      'nidn': nidn
    });
    if (res.statusCode == 200) {
      print('sukses');
    }

    return json.decode(res.body);
  }

  Future<List<MdlDosenPilihan>> fetchDosenSelect(String nim) async {
    var res = await http.get("$listDosbim" + '$nim');
    var dataDosen = jsonDecode(res.body);

    List<MdlDosenPilihan> listDosen = [];

    for (int i = 0; i < dataDosen.length; i++) {
      var modelDosen = MdlDosenPilihan(
        nidn: dataDosen[i]['nidn'],
        nama: dataDosen[i]['nama'],
      );
      listDosen.add(modelDosen);
    }
    return listDosen;
  }

  Future addPerbaikanPage(
      String hasilPerbaikan, String halamanNaskah, String idMonitoring) async {
    var res = await http.post("$addPerbaikan" + '$idMonitoring', body: {
      'hasil_perbaikan': hasilPerbaikan,
      'halaman': halamanNaskah,
    });

    if (res.statusCode == 200) {
      print('sukses');
    }
    return json.decode(res.body);
  }
}
