import 'dart:io';

import 'package:monitoring_skripsi/model/mdl_detail_profil.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:monitoring_skripsi/repository/repo_monitoring.dart';
import 'package:monitoring_skripsi/repository/repo_profil.dart';
import 'package:monitoring_skripsi/service/pdf.dart';

import 'package:pdf/widgets.dart';

class PdfInvoice {
  List<MdlMonitoring> list = [];
  List<MdlDetailProfil> listProf = [];
  Future<File> generate(String nim, String name) async {
    var repo = await DosenRepo().validateMonitoring(nim);
    list = repo;

    var profil = await RepoProfil().fetchDetail(nim);
    listProf = profil;

    final Document pdf = Document(deflate: zlib.encode);
    pdf.addPage(
      MultiPage(
          orientation: PageOrientation.portrait,
          build: (context) => [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child: Text("LEMBAR MONITORING PELAKSANAAN TUGAS AKHIR",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold)),
                      ),
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('NAMA'),
                                  Text('NIM '),
                                  Text('Dosen Pembimbing I '),
                                  Text('Dosen Pembimbing II '),
                                  Text('Judul TA')
                                ]),
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(':'),
                                      Text(':'),
                                      Text(':'),
                                      Text(':'),
                                      Text(':')
                                    ])),
                            Expanded(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(name,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(nim,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(listProf[0].dosenPembimbing1 +
                                        "\n" +
                                        listProf[0].dosenPembimbing2),
                                    Text(listProf[0].judul),
                                  ]),
                            ),
                          ]),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Mulai TA                        :   " +
                                  listProf[0].tanggalMulai),
                              Text(
                                  "Selesai TA : " + listProf[0].tanggalSelesai),
                            ]),
                      ),
                    ]),
                Table.fromTextArray(
                    headerStyle: TextStyle(fontWeight: FontWeight.bold),
                    // headerDecoration: BoxDecoration(color: PdfColors.grey300),
                    // cellHeight: 30,
                    data: <List<dynamic>>[
                      ['Tanggal', 'Uraian', 'Status'],
                      ...list.map(
                        (e) => [
                          e.tangalBimbingan,
                          e.materiBimbingan,
                          // e.namaDosen,
                          // e.statusMonitoring,
                          'selesai'
                        ],
                      )
                    ]),
                SizedBox(height: 40),
                Padding(
                    padding: EdgeInsets.only(left: 100, bottom: 10),
                    child: Text('Yogyakarta,')),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Dosen Pembimbing I'),
                            SizedBox(height: 50),
                            Text(listProf[0].dosenPembimbing1)
                          ]),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Dosen Pembimbing II'),
                            SizedBox(height: 50),
                            Text(listProf[0].dosenPembimbing2)
                          ])
                    ]),
              ]),
    );

    return PdfApi.saveDocument(pdf: pdf);
  }
}
