import 'dart:convert';

import 'package:monitoring_skripsi/helper.dart';
import 'package:http/http.dart' as http;
import 'package:monitoring_skripsi/model/mdl_detail_profil.dart';

class ProfilService {
  String urlAdd = HelperUrl().addProfil();
  String urlgetProf = HelperUrl().getDetailProfil();
  Future addProfil(
      String nim,
      String judul,
      String tanggalMulai,
      String tanggalSelesai,
      String dospem1,
      String dospem2,
      String status1,
      String status2) async {
    var result = await http.post('$urlAdd', body: {
      'nim': nim,
      'judul': judul,
      'tanggal_mulai': tanggalMulai,
      'tanggal_selesai': tanggalSelesai,
      'nidn1': dospem1,
      'nidn2': dospem2,
      'status1': status1,
      'status2': status2
    });
    return json.decode(result.body);
  }

  Future<List<MdlDetailProfil>> fetchBimbingan(String nim) async {
    var res = await http.get(
      "$urlgetProf" + '$nim',
    );
    var dataBimbingan = jsonDecode(res.body);

    List<MdlDetailProfil> bimbingan = [];

    var modelMhs = MdlDetailProfil(
      idProfil: dataBimbingan['id_profil'],
      judul: dataBimbingan['judul'],
      dosenPembimbing1: dataBimbingan['Dosen Pembimbing 1'],
      dosenPembimbing2: dataBimbingan['Dosen Pembimbing 2'],
      tanggalMulai: dataBimbingan['tanggal_mulai'],
      tanggalSelesai: dataBimbingan['tanggal_selesai'],
      statusBimbingan: dataBimbingan['status_bimbingan'],
    );
    bimbingan.add(modelMhs);

    //print(dataBimbingan);

    return bimbingan;
  }
}
