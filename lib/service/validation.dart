class Validation {
  String validateEmail(String value) {
    if (!value.contains('@mail.umy.ac.id') &&
        !value.contains('@ft.umy.ac.id')) {
      //JIKA VALUE MENGANDUNG KARAKTER @
      return 'Email tidak valid'; //MAKA PESAN DITAMPILKAN
    }
    return null;
  }

  String validateName(String value) {
    if (value.isEmpty) {
      //JIKA VALUE KOSONG
      return 'Nama Lengkap Harus Diisi'; //MAKA PESAN DITAMPILKAN
    }
    return null;
  }

  String validateNIM(String value) {
    if (value.isEmpty) {
      //JIKA VALUE KOSONG
      return 'NIM Harus Diisi'; //MAKA PESAN DITAMPILKAN
    } else if (value.length < 11) {
      return 'NIM Tidak Boleh Kurang dari 11';
    }

    return null;
  }

  String validatePassword(String value) {
    //MENERIMA VALUE DENGAN TYPE STRING
    if (value.length < 4) {
      //VALUE TERSEBUT DI CEK APABILA KURANG DARI 6 KARAKTER
      return 'Password Minimal 4 Karakter'; //MAKA ERROR AKAN DITAMPILKAN
    }
    return null; //SELAIN ITU LOLOS VALIDASI
  }

  String validateNoHp(String value) {
    //MENERIMA VALUE DENGAN TYPE STRING
    if (value.length < 11 || value.length > 13) {
      //VALUE TERSEBUT DI CEK APABILA KURANG DARI 6 KARAKTER
      return 'No Hp Tidak Valid'; //MAKA ERROR AKAN DITAMPILKAN
    }
    return null; //SELAIN ITU LOLOS VALIDASI
  }
}
