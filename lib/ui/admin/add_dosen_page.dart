import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/bloc/bloc.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';

import '../../constant.dart';

import 'homepage_admin.dart';

class AddDosenPage extends StatefulWidget {
  final nama = TextEditingController();
  final nip = TextEditingController();
  final email = TextEditingController();
  final keahlian = TextEditingController();
  final password = TextEditingController();
  final nohp = TextEditingController();
  final bloc = BlocListMhs();
  @override
  _AddDosenPageState createState() => _AddDosenPageState();
}

class _AddDosenPageState extends State<AddDosenPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
              child: Stack(
        children: [
          CostumeAppBar(
            size: null,
            judul: 'Add Data Dosen',
          ),
          Column(
            children: [
              SizedBox(
                height: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Form(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LabelForm(
                      label: 'Nama Lengkap',
                    ),
                    TextForm(
                      tf: widget.bloc.nama,
                      icon: Icon(
                        Icons.person,
                        color: hijau,
                      ),
                      benar: false,
                      hint: 'Masukan Nama Lengkap',
                      cr: widget.nama,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LabelForm(
                      label: 'NIDN',
                    ),
                    TextFormDigit(
                      icon: Icon(
                        Icons.confirmation_num,
                        color: hijau,
                      ),
                      benar: false,
                      cr: widget.nip,
                      hint: 'Masukan NIDN',
                      tf: widget.bloc.nidn,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LabelForm(
                      label: 'Keahlian',
                    ),
                    TextForm(
                      icon: Icon(
                        Icons.person,
                        color: hijau,
                      ),
                      benar: false,
                      hint: 'Masukan Keahlian',
                      cr: widget.keahlian,
                      tf: widget.bloc.keahlian,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LabelForm(
                      label: 'E-mail',
                    ),
                    TextForm(
                      icon: Icon(
                        Icons.mail,
                        color: hijau,
                      ),
                      benar: false,
                      cr: widget.email,
                      hint: 'Masukan E-Mail',
                      tf: widget.bloc.email,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LabelForm(
                      label: 'Password',
                    ),
                    TextFormPass(
                      icon: Icon(
                        Icons.lock,
                        color: hijau,
                      ),
                      benar: true,
                      cr: widget.password,
                      hint: 'Masukan Password',
                      tf: widget.bloc.password,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LabelForm(
                      label: 'No HP',
                    ),
                    TextFormDigit(
                      icon: Icon(Icons.phone, color: hijau),
                      benar: false,
                      cr: widget.nohp,
                      hint: 'Masukan No HP',
                      tf: widget.bloc.nohp,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    DefaultButton(
                      ontap: () {
                        widget.bloc.blocAddDosen();
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => HomeAdmin()),
                        );
                      },
                      ket: 'Submit',
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                )),
              )
            ],
          ),
        ],
      ))),
    );
  }
}
