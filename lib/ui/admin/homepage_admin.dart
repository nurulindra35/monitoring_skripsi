import 'dart:async';
import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/bloc/bloc.dart';
import 'package:monitoring_skripsi/ui/admin/add_dosen_page.dart';
import 'package:monitoring_skripsi/ui/login_page/login_page.dart';

import '../../constant.dart';

class HomeAdmin extends StatefulWidget {
  final bloc = BlocListMhs();
  @override
  _HomeAdminState createState() => _HomeAdminState();
}

class _HomeAdminState extends State<HomeAdmin> {
  Timer timer;
  @override
  void initState() {
    widget.bloc.fetcchAllDosen();
    timer = Timer.periodic(
        Duration(milliseconds: 5000), (timer) => widget.bloc.fetcchAllDosen());

    super.initState();
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    if (timer.isActive) timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              height: size.height / 4,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: hijau,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25))),

              // unntuk menampulkan list dosen
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  'images/header.png',
                  width: double.infinity,
                  //height: 20,
                  fit: BoxFit.fitWidth,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                        padding:
                            EdgeInsets.only(top: 10, right: 10, bottom: 20),
                        child: IconButton(
                            icon: Icon(
                              Icons.logout,
                              size: 35.0,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPage()),
                              );
                            }))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Selamat Datang",
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Text(
                        'Admin',
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30, bottom: 10),
                  child: Text(
                    'Daftar Dosen : ',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                Expanded(
                  child: StreamBuilder(
                    stream: widget.bloc.dataDosen,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            return Card(
                              margin: EdgeInsets.all(15),
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    height: 10,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Nama       : ' + '',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Flexible(
                                          child: Text(
                                            snapshot.data[index].nama,
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'NIDN        : ',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Flexible(
                                          child: Text(
                                            snapshot.data[index].nidn,
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Keahlian  : ' + '',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Flexible(
                                          child: Text(
                                            snapshot.data[index].keahlian,
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'No HP      : ' + '',
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Flexible(
                                          child: Text(
                                            snapshot.data[index].nohp,
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // Row(
                                  //   //crossAxisAlignment: CrossAxisAlignment.center,
                                  //   mainAxisAlignment: MainAxisAlignment.end,
                                  //   children: [
                                  //     IconButton(
                                  //         icon: Icon(
                                  //           Icons.delete,
                                  //           color: hijau,
                                  //         ),
                                  //         onPressed: null),
                                  //     IconButton(
                                  //         icon: Icon(
                                  //           Icons.mode_edit,
                                  //           color: hijau,
                                  //         ),
                                  //         onPressed: () {
                                  //           String nip =
                                  //               snapshot.data[index].nip;
                                  //           String nama =
                                  //               snapshot.data[index].nama;
                                  //           String keahlian =
                                  //               snapshot.data[index].keahlian;
                                  //           String email =
                                  //               snapshot.data[index].email;
                                  //           String pass =
                                  //               snapshot.data[index].password;
                                  //           String nohp =
                                  //               snapshot.data[index].nohp;
                                  //           Navigator.push(
                                  //             context,
                                  //             MaterialPageRoute(
                                  //                 builder: (context) =>
                                  //                     EditDosen(
                                  //                       nipds: nip,
                                  //                       namads: nama,
                                  //                       keahliands: keahlian,
                                  //                       emailds: email,
                                  //                       passwordds: pass,
                                  //                       nohpds: nohp,
                                  //                     )),
                                  //           );
                                  //         }),
                                  //   ],
                                  // ),
                                  // SizedBox(
                                  //   width: double.infinity,
                                  //   height: 10,
                                  // ),
                                ],
                              ),
                            );
                          },
                        );
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 30,
              right: 30,
              child: FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AddDosenPage()),
                  );
                },
                backgroundColor: hijau,
                child: Icon(Icons.add),
              ),
            ),
            //Untuk menampilkan data
          ],
        ),
      ),
    );
  }
}
