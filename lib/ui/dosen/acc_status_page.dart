import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/formDetailData.dart';

import 'data_monitoring_dosen.dart';

class DsAcc extends StatefulWidget {
  final int idMonitoring;
  final String nim;
  final String nip;
  final String name;
  final String tanggal;
  final String materi;
  final String namaDS;
  final String komentar;
  final String hasilPerbaikan;
  final String halaman;
  const DsAcc(
      {Key key,
      this.idMonitoring,
      this.nim,
      this.nip,
      this.name,
      this.tanggal,
      this.materi,
      this.namaDS,
      this.komentar,
      this.hasilPerbaikan,
      this.halaman})
      : super(key: key);
  @override
  _DsAccState createState() => _DsAccState();
}

class _DsAccState extends State<DsAcc> {
  String status = 'ACC';
  String status2 = 'Reject';
  // List<MdlDetailMonitoring> list = [];
  // Future fetch() async {
  //   var repo = await RepofetchMonitoring()
  //       .fetchDetail(widget.idMonitoring, widget.nim);
  //   setState(() {
  //     list = repo;
  //   });
  // }

  // @override
  // void initState() {
  //   fetch();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CostumeAppBar(
            size: size,
            judul: 'Validasi Monitoring',
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 10, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormDetailData(
                  judul: 'Tanggal                 : ',
                  data: widget.tanggal,
                ),
                FormDetailData(
                  judul: 'Keterangan           : ',
                  data: widget.materi,
                ),
                FormDetailData(
                  judul: 'Komentar              :',
                  data: widget.komentar ?? '',
                ),
                FormDetailData(
                  judul: 'Hasil Perbaikan    :',
                  data: widget.hasilPerbaikan ?? '',
                ),
                FormDetailData(
                  judul: 'Halaman Naskah  :',
                  data: widget.halaman ?? '',
                ),
                SizedBox(
                  height: 20,
                ),
                DefaultButton(
                  ket: 'ACC',
                  ontap: () {
                    DosenRepo().updateStatus(status, widget.idMonitoring);
                    // Navigator.popUntil(context, (route) => route.isCurrent);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DsDataMonitoring(
                                  nim: widget.nim,
                                  nip: widget.nip,
                                  name: widget.name,
                                )));
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                DefaultButton2(
                  ket: 'Reject',
                  ontap: () {
                    DosenRepo().updateStatus(status2, widget.idMonitoring);
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DsDataMonitoring(
                                  nim: widget.nim,
                                  nip: widget.nip,
                                  name: widget.name,
                                )));
                  },
                )
              ],
            ),
          ),
        ],
      )),
    );
  }
}
