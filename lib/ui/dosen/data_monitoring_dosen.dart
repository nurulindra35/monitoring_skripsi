import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/detail_monitoring_page.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';

import '../../constant.dart';
import 'acc_status_page.dart';
import 'komentar_page.dart';
import 'menu_page.dart';

class DsDataMonitoring extends StatefulWidget {
  final String nip;
  final String nim;
  final String name;

  const DsDataMonitoring({Key key, this.nip, this.nim, this.name})
      : super(key: key);

  @override
  _DsDataMonitoring createState() => _DsDataMonitoring();
}

class _DsDataMonitoring extends State<DsDataMonitoring> {
  List<MdlMonitoring> list = [];
  Future fetch() async {
    var repo = await DosenRepo().fetchMntr(widget.nip, widget.nim);
    setState(() {
      list = repo;
    });
  }

  @override
  void initState() {
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          CostumeAppBar(
            size: size,
            judul: 'Data Monitoring',
            // tf: () {
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //           builder: (context) => MenuMhs(
            //                 nim: widget.nim,
            //                 nip: widget.nip,
            //                 nama: widget.name,
            //               )));
            // },
          ),
          list.length > 0
              ? RefreshIndicator(
                  onRefresh: () async {
                    await fetch();
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: 60),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Row(
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                columns: <DataColumn>[
                                  DataColumn(
                                      label: Container(
                                    child: Text("Tanggal"),
                                  )),
                                  DataColumn(label: Text("Keterangan")),
                                  // DataColumn(label: Text("Dosen")),
                                  DataColumn(label: Text("Status")),
                                ],
                                rows:
                                    list // Loops through dataColumnText, each iteration assigning the value to element
                                        .map(
                                          ((element) => DataRow(
                                                cells: <DataCell>[
                                                  DataCell(Text(element
                                                      .tangalBimbingan)), //Extracting from Map element the value
                                                  DataCell(Text(
                                                      element.materiBimbingan)),
                                                  // DataCell(Text(element.namaDosen)),
                                                  DataCell(Row(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          IconButton(
                                                            icon: Icon(
                                                                Icons.details),
                                                            onPressed: () {
                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) =>
                                                                          DetailMoonitoringPage(
                                                                            idMonitoring:
                                                                                element.idMonitoring,
                                                                            nim:
                                                                                widget.nim,
                                                                            name:
                                                                                widget.name,
                                                                            tanggal:
                                                                                element.tangalBimbingan,
                                                                            materi:
                                                                                element.materiBimbingan,
                                                                            namaDS:
                                                                                element.namaDosen,
                                                                            komentar:
                                                                                element.komentar,
                                                                            hasilPerbaikan:
                                                                                element.hasilPerbaikan,
                                                                            halaman:
                                                                                element.halaman,
                                                                            status:
                                                                                element.statusMonitoring,
                                                                          )));
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                      if (element.komentar !=
                                                              null ||
                                                          element.statusMonitoring ==
                                                              'ACC' ||
                                                          element.statusMonitoring ==
                                                              'Reject')
                                                        IconButton(
                                                            icon: Icon(
                                                              Icons.edit,
                                                            ),
                                                            onPressed: null)
                                                      else
                                                        IconButton(
                                                          icon: Icon(
                                                            Icons.edit,
                                                          ),
                                                          onPressed: () {
                                                            Navigator
                                                                .pushReplacement(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) =>
                                                                            DsKomentarMonitoring(
                                                                              id: element.idMonitoring,
                                                                              nim: widget.nim,
                                                                              nip: widget.nip,
                                                                              name: widget.name,
                                                                              keterangan: element.materiBimbingan,
                                                                              tanggal: element.tangalBimbingan,
                                                                            )));
                                                          },
                                                        ),
                                                      if (element
                                                              .statusMonitoring ==
                                                          'ACC')
                                                        Icon(
                                                          Icons.check_box,
                                                          color: hijau,
                                                        )
                                                      else if (element
                                                              .statusMonitoring ==
                                                          'Reject')
                                                        Icon(
                                                          Icons
                                                              .cancel_presentation,
                                                          color: Colors.red,
                                                        )
                                                      else if (element
                                                              .hasilPerbaikan !=
                                                          '')
                                                        Material(
                                                          color: Colors
                                                              .transparent,
                                                          child: InkWell(
                                                            hoverColor: Colors
                                                                .transparent,
                                                            onTap: () {
                                                              Navigator.pushReplacement(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) => DsAcc(
                                                                            idMonitoring:
                                                                                element.idMonitoring,
                                                                            nim:
                                                                                widget.nim,
                                                                            nip:
                                                                                widget.nip,
                                                                            name:
                                                                                widget.name,
                                                                            tanggal:
                                                                                element.tangalBimbingan,
                                                                            materi:
                                                                                element.materiBimbingan,
                                                                            komentar:
                                                                                element.komentar,
                                                                            hasilPerbaikan:
                                                                                element.hasilPerbaikan,
                                                                            halaman:
                                                                                element.halaman,
                                                                          )));
                                                            },
                                                            child: Container(
                                                              width: 40,
                                                              height: 20,
                                                              child: Center(
                                                                  child: Text(
                                                                      'Need')),
                                                              decoration: BoxDecoration(
                                                                  color: hijau,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              20)),
                                                            ),
                                                          ),
                                                        ),
                                                    ],
                                                  )),
                                                ],
                                              )),
                                        )
                                        .toList(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Center(
                  child: Text('Data Belum Tersedia'),
                ),
        ],
      )),
    );
  }
}
