import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/bloc/blocDosen.dart';
import 'package:monitoring_skripsi/ui/login_page/login_page.dart';

import '../../constant.dart';
import 'menu_page.dart';

class HomeScreenDosen extends StatefulWidget {
  final String name;
  final String nip;
  // final String nim;
  final bloc = BlocDosen();
  // final String nim;

  HomeScreenDosen({
    Key key,
    this.nip,
    this.name,
  }) : super(key: key);

  @override
  _HomeScreenDosenState createState() => _HomeScreenDosenState();
}

class _HomeScreenDosenState extends State<HomeScreenDosen> {
  @override
  void initState() {
    widget.bloc.fetcchAllMhs(widget.nip);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          ListView(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                'images/header.png',
                width: double.infinity,
                fit: BoxFit.fitWidth,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10, right: 10, bottom: 20),
                    child: IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                        );
                      },
                      icon: Icon(
                        Icons.exit_to_app,
                        size: 35.0,
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Selamat Datang",
                      style:
                          TextStyle(fontSize: 34, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      widget.name,
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: hijau),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, bottom: 10),
                child: Center(
                  child: Text(
                    'Daftar Mahasiswa Bimbingan : ',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: hijau),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                height: size.height,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: hijau,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),

                // unntuk menampulkan list mahasiswa

                child: StreamBuilder(
                  stream: widget.bloc.listMhs,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                String nim = snapshot.data[index].nim;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MenuMhs(
                                              nama: widget.name,
                                              nim: nim,
                                              nip: widget.nip,
                                            )));
                              },
                              child: Card(
                                margin: EdgeInsets.only(
                                    bottom: 5, top: 10, left: 20, right: 20),
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 5),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Nama      : ',
                                              style: TextStyle(
                                                fontSize: 18,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            Flexible(
                                              child: Text(
                                                snapshot.data[index].nama,
                                                style: TextStyle(
                                                  fontSize: 18,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 5),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'NIM         : ' + '',
                                              style: TextStyle(
                                                fontSize: 18,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                            Flexible(
                                              child: Text(
                                                snapshot.data[index].nim,
                                                style: TextStyle(
                                                  fontSize: 18,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      // Padding(
                                      //   padding: const EdgeInsets.symmetric(
                                      //       vertical: 5, horizontal: 10),
                                      //   child: Row(
                                      //     crossAxisAlignment:
                                      //         CrossAxisAlignment.start,
                                      //     children: [
                                      //       Text(
                                      //         'Judul       : ' + '',
                                      //         style: TextStyle(
                                      //           fontSize: 18,
                                      //         ),
                                      //         textAlign: TextAlign.center,
                                      //       ),
                                      //       Flexible(
                                      //         child: Text(
                                      //           snapshot.data[index].judul,
                                      //           style: TextStyle(
                                      //             fontSize: 18,
                                      //           ),
                                      //           textAlign: TextAlign.left,
                                      //         ),
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                      // Padding(
                                      //   padding: const EdgeInsets.symmetric(
                                      //       vertical: 5, horizontal: 10),
                                      //   child: Row(
                                      //     crossAxisAlignment:
                                      //         CrossAxisAlignment.start,
                                      //     children: [
                                      //       Text(
                                      //         'Status     : ' + '',
                                      //         style: TextStyle(
                                      //           fontSize: 18,
                                      //         ),
                                      //         textAlign: TextAlign.center,
                                      //       ),
                                      //       Flexible(
                                      //         child: Text(
                                      //           snapshot.data[index].status,
                                      //           style: TextStyle(
                                      //             fontSize: 18,
                                      //           ),
                                      //           textAlign: TextAlign.left,
                                      //         ),
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
