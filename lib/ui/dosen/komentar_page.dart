import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/formDetailData.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';

import '../../constant.dart';
import 'data_monitoring_dosen.dart';

class DsKomentarMonitoring extends StatelessWidget {
  final komentar = TextEditingController();
  final String nip;
  final String nim;
  final int id;
  final String tanggal;
  final String keterangan;
  final String name;

  DsKomentarMonitoring(
      {Key key,
      this.nip,
      this.nim,
      this.id,
      this.tanggal,
      this.keterangan,
      this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CostumeAppBar(
              size: size,
              judul: 'Edit Data',
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: 20, bottom: 10, left: 30, right: 30),
              child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FormDetailData(
                        judul: 'Tanggal                 : ', data: tanggal),
                    FormDetailData(
                      judul: 'Keterangan           : ',
                      data: keterangan,
                    ),
                    SizedBox(
                      height: size.height / 38,
                    ),
                    LabelForm(
                      label: 'Komentar',
                    ),
                    TextForm(
                      icon: Icon(
                        Icons.comment,
                        color: hijau,
                      ),
                      benar: false,
                      hint: 'Masukan Komentar',
                      cr: komentar,
                    ),
                    SizedBox(
                      height: size.height / 38,
                    ),
                    DefaultButton(
                      ket: 'Submit',
                      ontap: () {
                        DosenRepo().komen(komentar.text, id);
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DsDataMonitoring(
                                      nim: nim,
                                      nip: nip,
                                      name: name,
                                    )));
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
