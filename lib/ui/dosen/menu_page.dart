import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/ui/dosen/validate_monitoring_page.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';

import 'data_monitoring_dosen.dart';
import 'home_screen_dosen.dart';

class MenuMhs extends StatelessWidget {
  final String nim;
  final String nip;
  final String nama;

  const MenuMhs({Key key, this.nim, this.nip, this.nama}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: DefaultButton(
                  ket: 'Data Monitoring',
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DsDataMonitoring(
                                  nim: nim,
                                  nip: nip,
                                  name: nama,
                                )));
                    // print(nama);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: DefaultButton(
                  ket: 'Data Monitoring Tervalidasi',
                  ontap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ValidateMonitoringPage(
                                  nip: nip,
                                  name: nama,
                                  nim: nim,
                                )));
                  },
                ),
              ),
            ],
          ),
          CostumeAppBar(
            size: null,
            judul: 'Data Monitoring',
          ),
        ],
      )),
    );
  }
}
