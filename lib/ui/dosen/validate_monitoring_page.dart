import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/constant.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/repository/dosen_repo.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/detail_monitoring_page.dart';

import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';

import 'menu_page.dart';

class ValidateMonitoringPage extends StatefulWidget {
  final String nip;
  final String nim;
  final String name;

  const ValidateMonitoringPage({Key key, this.nip, this.nim, this.name})
      : super(key: key);

  @override
  _ValidateMonitoringPage createState() => _ValidateMonitoringPage();
}

class _ValidateMonitoringPage extends State<ValidateMonitoringPage> {
  List<MdlMonitoring> list = [];
  Future fetch() async {
    var repo = await DosenRepo().validateMonitoring(widget.nim);
    setState(() {
      list = repo;
    });
  }

  @override
  void initState() {
    print(widget.name);
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          // Positioned(
          //     top: size.height / 48,
          //     right: size.width / 28,
          //     child: IconButton(
          //       onPressed: () {},
          //       icon: Icon(
          //         Icons.print,
          //         color: hijau,
          //       ),
          //     )),
          Column(
            children: [
              CostumeAppBar(
                size: size,
                judul: 'Data Monitoring',
                // tf: () {
                //   Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //           builder: (context) => MenuMhs(
                //                 nim: widget.nim,
                //                 nip: widget.nip,
                //                 nama: widget.name,
                //               )));
                // },
              ),
              Row(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(
                        columns: <DataColumn>[
                          DataColumn(
                              label: Container(
                            child: Text("Tanggal"),
                          )),
                          DataColumn(label: Text("Keterangan")),
                          // DataColumn(label: Text("Dosen")),
                          DataColumn(label: Text("Status")),
                        ],
                        rows:
                            list // Loops through dataColumnText, each iteration assigning the value to element
                                .map(
                                  ((element) => DataRow(
                                        cells: <DataCell>[
                                          DataCell(Text(element
                                              .tangalBimbingan)), //Extracting from Map element the value
                                          DataCell(
                                              Text(element.materiBimbingan)),
                                          // DataCell(Text(element.namaDosen)),
                                          DataCell(
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.check_box,
                                                  color: hijau,
                                                ),
                                                Column(
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(Icons.details),
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        DetailMoonitoringPage(
                                                                          idMonitoring:
                                                                              element.idMonitoring,
                                                                          nim: widget
                                                                              .nim,
                                                                          name:
                                                                              widget.name,
                                                                          tanggal:
                                                                              element.tangalBimbingan,
                                                                          materi:
                                                                              element.materiBimbingan,
                                                                          namaDS:
                                                                              element.namaDosen,
                                                                          komentar:
                                                                              element.komentar,
                                                                          hasilPerbaikan:
                                                                              element.hasilPerbaikan,
                                                                          halaman:
                                                                              element.halaman,
                                                                          status:
                                                                              element.statusMonitoring,
                                                                        )));
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )),
                                )
                                .toList(),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ],
      )),
    );
  }
}
