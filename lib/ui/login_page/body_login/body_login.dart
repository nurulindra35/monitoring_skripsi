import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/repository/auth_repo.dart';
import 'package:monitoring_skripsi/service/validation.dart';
import 'package:monitoring_skripsi/ui/admin/homepage_admin.dart';
import 'package:monitoring_skripsi/ui/dosen/home_screen_dosen.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/home_page_mahasiswa.dart';
import 'package:monitoring_skripsi/ui/register_page/register_page.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/keterangan_form.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../constant.dart';

class BodyLogin extends StatefulWidget {
  BodyLogin({
    Key key,
  }) : super(key: key);

  @override
  _BodyLoginState createState() => _BodyLoginState();
}

class _BodyLoginState extends State<BodyLogin> with Validation {
  final _repoLogin = AuthRepo();
  final formKey = GlobalKey<FormState>();
  final password = TextEditingController();
  final email = TextEditingController();
  String dropdownValue;

  ScaffoldState scaffoldState;
  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
      child: Form(
        key: formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LabelForm(
              label: 'E-Mail',
            ),
            TextForm(
              val: validateEmail,
              icon: Icon(Icons.mail, color: hijau),
              benar: false,
              cr: email,
              hint: 'Masukan E-Mail UMY',
            ),
            SizedBox(
              height: 30,
            ),
            LabelForm(
              label: 'Password',
            ),
            TextFormPass(
              val: validatePassword,
              icon: Icon(Icons.lock, color: hijau),
              benar: true,
              hint: 'Masukan Password',
              cr: password,
            ),
            SizedBox(
              height: 30,
            ),
            DefaultButton(
              ontap: () {
                if (formKey.currentState.validate()) {
                  login();
                }
              },
              ket: 'Login',
            ),
            KeteranganForm(
              ket1: 'Belum Punya Akun? ',
              ket2: 'Register Disini',
              ontap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RegisterPage()));
              },
            ),
          ],
        ),
      ),
    );
  }

  void login() async {
    var datauser = await _repoLogin.repoLogin(email.text, password.text);
    // if (datauser['role'] == 'Admin') {
    //   print(datauser['nama']);
    // }

    if (datauser['email'] == null && datauser['password'] == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: datauser['message'],
        ),
      );
    } else if (datauser['role'] == 'Mahasiswa' && datauser['email'] != null) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(
                    name: datauser['nama'],
                    nim: datauser['nim'],
                  )));
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Selamat Datang ' + datauser['nama'],
        ),
      );
    } else if (datauser['role'] == 'Dosen' && datauser['email'] != null) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreenDosen(
                    name: datauser['nama'],
                    nip: datauser['nidn'],
                  )));
      print(datauser['nidn']);
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Selamat Datang ' + datauser['nama'],
        ),
      );
    } else if (datauser['role'] == 'Admin' && datauser['email'] != null) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeAdmin()));
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Selamat Datang ' + datauser['nama'],
        ),
      );
    }
  }
}
