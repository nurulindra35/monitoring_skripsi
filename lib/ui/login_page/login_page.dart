import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/ui/widgets/header.dart';

import 'body_login/body_login.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderAuth(
                auth: 'Login',
              ),
              BodyLogin()
            ],
          ),
        ),
      ),
    );
  }
}