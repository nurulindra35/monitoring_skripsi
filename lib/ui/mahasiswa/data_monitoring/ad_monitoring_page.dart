import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_dosen_pilihan.dart';
import 'package:monitoring_skripsi/repository/repo_monitoring.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/datepicker.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';

import '../../../constant.dart';
import 'data_monitoring_mhs_page.dart';

class AddMonitoringPage extends StatefulWidget {
  final tanggal2 = TextEditingController();
  final keterangan = TextEditingController();
  final String nim;
  final String name;
  final String idProfil;
  AddMonitoringPage({
    Key key,
    this.nim,
    this.name,
    this.idProfil,
  }) : super(key: key);

  @override
  _AddMonitoringPageState createState() => _AddMonitoringPageState();
}

class _AddMonitoringPageState extends State<AddMonitoringPage> {
  DateTime selectedDate2 = DateTime.now();
  String dosbim2;

  //untuk menMPILKAN DOSEN
  List<MdlDosenPilihan> listDosen = [];
  Future fetchDosenPilihan() async {
    var dataDosen = await RepoMonitoring().fetchDosenpilihan(widget.nim);
    setState(() {
      listDosen = dataDosen;
    });
  }

  @override
  void initState() {
    fetchDosenPilihan();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            CostumeAppBar(
              size: MediaQuery.of(context).size,
              judul: 'Add Monitoring',
            ),
            Padding(
              padding: EdgeInsets.only(left: 30, right: 30, top: 30),
              child: Form(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LabelForm(
                    label: 'Dosen Pembimbing',
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 3,
                            offset: Offset(0, 5), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(30)),
                    child: DropdownButton<String>(
                      hint: Container(
                        width: size.width / 2,
                        child: Text(
                          "Pilih Dosen Pembimbing ",
                          style: TextStyle(color: Colors.grey),
                          textAlign: TextAlign.end,
                        ),
                      ),
                      value: dosbim2,
                      icon: Padding(
                        padding: const EdgeInsets.only(left: 70),
                        child: Icon(
                          Icons.arrow_drop_down,
                          color: hijau,
                        ),
                      ),
                      iconSize: 40,
                      underline: SizedBox(),
                      onChanged: (String newValue) {
                        setState(() {
                          dosbim2 = newValue;
                        });
                      },
                      items: listDosen.map((list) {
                        return DropdownMenuItem(
                          child: Text(list.nama),
                          value: list.nidn,
                        );
                      }).toList(),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  LabelForm(
                    label: 'Tanggal',
                  ),
                  DatePickerWidget(
                    ctr: widget.tanggal2,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  LabelForm(
                    label: 'Materi Bimbingan',
                  ),
                  TextForm(
                    icon: Icon(
                      Icons.info,
                      color: hijau,
                    ),
                    benar: false,
                    length: 20,
                    hint: 'Input Materi Bimbingan',
                    cr: widget.keterangan,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  DefaultButton(
                    ket: 'Submit',
                    ontap: makan,
                  )
                ],
              )),
            ),
          ],
        ),
      )),
    );
  }

  makan() async {
    RepoMonitoring().addMonitoring(
        widget.idProfil, widget.tanggal2.text, widget.keterangan.text, dosbim2);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => DataMonitoringMhs(
                  nim: widget.nim,
                  name: widget.name,
                  idProfil: widget.idProfil,
                )));
    await RepoMonitoring().getMonitoring(widget.nim);
  }
}
