import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/repository/repo_monitoring.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/data_monitoring_mhs_page.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/formDetailData.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';

import '../../../constant.dart';

class AddPerbaikanPage extends StatefulWidget {
  final String idProfil;
  final String idMonitoring;
  final String tanggal;
  final String keterangan;
  final String komentar;
  final String nim;
  final String name;

  const AddPerbaikanPage(
      {Key key,
      this.idMonitoring,
      this.tanggal,
      this.keterangan,
      this.komentar,
      this.nim,
      this.name,
      this.idProfil})
      : super(key: key);

  @override
  _AddPerbaikanPageState createState() => _AddPerbaikanPageState();
}

class _AddPerbaikanPageState extends State<AddPerbaikanPage> {
  final perbaikan = TextEditingController();
  final halaman = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CostumeAppBar(
              size: size,
              judul: 'Data Perbaikan',
              // tf: () {
              //   Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //           builder: (context) => DataMonitoringMhs(
              //                 nim: widget.nim,
              //                 name: widget.name,
              //                 idProfil: widget.idProfil,
              //               )));
              // },
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: 20, bottom: 10, left: 30, right: 30),
              child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FormDetailData(
                      judul: 'Tanggal                 : ',
                      data: widget.tanggal,
                    ),
                    FormDetailData(
                      judul: 'Keterangan           : ',
                      data: widget.keterangan,
                    ),
                    FormDetailData(
                      judul: 'Komentar              : ',
                      data: widget.komentar,
                    ),
                    SizedBox(
                      height: size.height / 38,
                    ),
                    LabelForm(
                      label: 'Hasil Perbaikan',
                    ),
                    TextForm(
                      icon: Icon(
                        Icons.self_improvement,
                        color: hijau,
                      ),
                      benar: false,
                      hint: 'Masukan  Hasil Perbaikan',
                      cr: perbaikan,
                    ),
                    SizedBox(
                      height: size.height / 38,
                    ),
                    LabelForm(
                      label: 'Halaman Naskah',
                    ),
                    TextFormDigit(
                      icon: Icon(
                        Icons.pages,
                        color: hijau,
                      ),
                      benar: false,
                      hint: 'Masukan Halaman Naskah Skripsi',
                      cr: halaman,
                    ),
                    SizedBox(
                      height: size.height / 38,
                    ),
                    DefaultButton(
                      ket: 'Submit',
                      ontap: () {
                        RepoMonitoring().repoEditMonitoring(
                            perbaikan.text, halaman.text, widget.idMonitoring);
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DataMonitoringMhs(
                                      nim: widget.nim,
                                      name: widget.name,
                                      idProfil: widget.idProfil,
                                    )));
                      },
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
