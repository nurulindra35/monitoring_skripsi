import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_monitoring.dart';
import 'package:monitoring_skripsi/repository/repo_monitoring.dart';
import 'package:monitoring_skripsi/service/pdf.dart';
import 'package:monitoring_skripsi/service/pdf_invoice.dart';

import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/ad_monitoring_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/add_perbaikan_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/detail_monitoring_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/home_page_mahasiswa.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';

import '../../../constant.dart';

class DataMonitoringMhs extends StatefulWidget {
  final String nim;
  final String name;
  final String idProfil;
  @override
  _DataMonitoringMhsState createState() => _DataMonitoringMhsState();

  DataMonitoringMhs({
    Key key,
    this.nim,
    this.name,
    this.idProfil,
  }) : super(key: key);
}

class _DataMonitoringMhsState extends State<DataMonitoringMhs> {
  bool rememberMe = false;

  List<MdlMonitoring> list = [];
  Future fetch() async {
    var repo = await RepoMonitoring().getMonitoring(widget.nim);
    setState(() {
      list = repo;
    });
    print(list.length);
  }

  @override
  void initState() {
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Positioned(
              top: size.height / 48,
              right: size.width / 28,
              child: IconButton(
                onPressed: () async {
                  final pdfFile =
                      await PdfInvoice().generate(widget.nim, widget.name);
                  PdfApi.openFile(pdfFile);
                },
                icon: Icon(
                  Icons.print,
                  color: hijau,
                ),
              )),
          CostumeAppBar2(
            size: size,
            judul: 'Data Monitoring',
            tf: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeScreen(
                            name: widget.name,
                            nim: widget.nim,
                          )));
            },
          ),
          RefreshIndicator(
            onRefresh: () async {
              await fetch();
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 60),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Row(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          columns: <DataColumn>[
                            DataColumn(
                                label: Container(
                              child: Text("Tanggal"),
                            )),
                            DataColumn(label: Text("keterangan")),
                            // DataColumn(label: Text("Dosen")),
                            DataColumn(label: Text("Status")),
                          ],
                          rows:
                              list // Loops through dataColumnText, each iteration assigning the value to element
                                  .map(
                                    ((element) => DataRow(
                                          cells: <DataCell>[
                                            DataCell(Text(element
                                                .tangalBimbingan)), //Extracting from Map element the value
                                            DataCell(
                                                Text(element.materiBimbingan)),
                                            // DataCell(Text(element.namaDosen)),

                                            DataCell(Row(
                                              children: [
                                                if (element.statusMonitoring ==
                                                    'ACC')
                                                  // Text(
                                                  //   'ACC',
                                                  //   style: TextStyle(
                                                  //       color: hijau,
                                                  //       fontWeight:
                                                  //           FontWeight.bold),
                                                  // )
                                                  Icon(
                                                    Icons.check,
                                                    color: hijau,
                                                  )
                                                else if (element
                                                        .statusMonitoring ==
                                                    'Reject')
                                                  // Text(
                                                  //   'Reject',
                                                  //   style: TextStyle(
                                                  //       color: Colors.red,
                                                  //       fontWeight:
                                                  //           FontWeight.bold),
                                                  // )
                                                  Icon(
                                                    Icons.cancel_outlined,
                                                    color: Colors.red,
                                                  )
                                                else
                                                  Expanded(child: Text('')),
                                                if (element.statusMonitoring ==
                                                        'ACC' ||
                                                    element.statusMonitoring ==
                                                        'Reject')
                                                  IconButton(
                                                      icon: Icon(
                                                        Icons.edit,
                                                      ),
                                                      onPressed: null)
                                                else
                                                  IconButton(
                                                    icon: Icon(
                                                      Icons.edit,
                                                    ),
                                                    onPressed: element
                                                                    .komentar ==
                                                                null ||
                                                            element.hasilPerbaikan !=
                                                                null
                                                        ? null
                                                        : () {
                                                            Navigator
                                                                .pushReplacement(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) =>
                                                                            AddPerbaikanPage(
                                                                              nim: widget.nim,
                                                                              name: widget.name,
                                                                              idProfil: widget.idProfil,
                                                                              idMonitoring: element.idMonitoring.toString(),
                                                                              tanggal: element.tangalBimbingan,
                                                                              keterangan: element.materiBimbingan,
                                                                              komentar: element.komentar,
                                                                            )));
                                                          },
                                                  ),
                                                Column(
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                          Icons.details_sharp),
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        DetailMoonitoringPage(
                                                                          idMonitoring:
                                                                              element.idMonitoring,
                                                                          nim: widget
                                                                              .nim,
                                                                          name:
                                                                              widget.name,
                                                                          tanggal:
                                                                              element.tangalBimbingan,
                                                                          materi:
                                                                              element.materiBimbingan,
                                                                          namaDS:
                                                                              element.namaDosen,
                                                                          komentar:
                                                                              element.komentar,
                                                                          hasilPerbaikan:
                                                                              element.hasilPerbaikan,
                                                                          halaman:
                                                                              element.halaman,
                                                                          status:
                                                                              element.statusMonitoring,
                                                                        )));
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            )),
                                          ],
                                        )),
                                  )
                                  .toList(),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: size.height / 18,
              ),
            ],
          ),
          Positioned(
              bottom: size.height / 28,
              right: size.width / 28,
              child: FloatingActionButton(
                backgroundColor: hijau,
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddMonitoringPage(
                                name: widget.name,
                                nim: widget.nim,
                                idProfil: widget.idProfil,
                              )));
                },
                child: Icon(Icons.add),
              )),
        ],
      )),
    );
  }
}
