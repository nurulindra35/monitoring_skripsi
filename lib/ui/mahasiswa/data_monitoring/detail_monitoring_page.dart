import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/formDetailData.dart';

class DetailMoonitoringPage extends StatefulWidget {
  final int idMonitoring;
  final String nim;
  final String name;
  final String tanggal;
  final String materi;
  final String namaDS;
  final String komentar;
  final String hasilPerbaikan;
  final String halaman;
  final String status;

  const DetailMoonitoringPage(
      {Key key,
      this.idMonitoring,
      this.nim,
      this.name,
      this.tanggal,
      this.materi,
      this.namaDS,
      this.komentar,
      this.hasilPerbaikan,
      this.halaman,
      this.status})
      : super(key: key);

  @override
  _DetailMoonitoringPageState createState() => _DetailMoonitoringPageState();
}

class _DetailMoonitoringPageState extends State<DetailMoonitoringPage> {

  @override
  void initState() {
    // fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CostumeAppBar(
            size: size,
            judul: 'Detail Monitoring',
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 10, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormDetailData(
                    judul: 'Tanggal                  : ',
                    data: widget.tanggal ?? ''),
                FormDetailData(
                  judul: 'Materi                     : ',
                  data: widget.materi ?? '',
                ),
                FormDetailData(
                    judul: 'Nama Dosen         : ', data: widget.namaDS ?? ''),
                FormDetailData(
                    judul: 'Komentar               :',
                    data: widget.komentar ?? ''),
                FormDetailData(
                  judul: 'Hasil Perbaikan    : ',
                  data: widget.hasilPerbaikan ?? '',
                ),
                FormDetailData(
                  judul: 'Halaman Naskah  : ',
                  data: widget.halaman ?? '',
                ),
                FormDetailData(
                  judul: 'Status                     : ',
                  data: widget.status ?? '',
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
