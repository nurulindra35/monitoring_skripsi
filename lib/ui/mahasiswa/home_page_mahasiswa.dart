import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_detail_profil.dart';
import 'package:monitoring_skripsi/repository/repo_profil.dart';
import 'package:monitoring_skripsi/ui/login_page/login_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/data_monitoring/data_monitoring_mhs_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/profil_mahasiswa/detail_profil_mhs_page.dart';
import 'package:monitoring_skripsi/ui/mahasiswa/profil_mahasiswa/profil_mahasiswa_page.dart';
import 'package:monitoring_skripsi/ui/widgets/card_home.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../constant.dart';

class HomeScreen extends StatefulWidget {
  final String name;
  final String nim;

  const HomeScreen({
    Key key,
    this.nim,
    this.name,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<MdlDetailProfil> list = [];
  Future<List<MdlDetailProfil>> fetch() async {
    var repo = await RepoProfil().fetchDetail(widget.nim);
    setState(() {
      list = repo;
    });
    return repo;
  }

  @override
  void initState() {
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    'images/header.png',
                    width: double.infinity,
                    fit: BoxFit.fitWidth,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                          padding:
                              EdgeInsets.only(top: 10, right: 10, bottom: 20),
                          child: IconButton(
                              icon: Icon(
                                Icons.outbox,
                                color: hijau,
                                size: 35.0,
                              ),
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginPage()),
                                );
                              }))
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Selamat Datang",
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.name,
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: hijau),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 80,
                  ),
                  Container(
                    height: size.height,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: hijau,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25))),
                    child: ListView(
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        CardHome(
                          tap: () {
                            list.length == 0
                                ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Profil(
                                              nama: widget.name,
                                              nim: widget.nim,
                                            )))
                                : Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailProfilPage(
                                              nama: widget.name,
                                              nim: widget.nim,
                                            )));
                          },
                          judul: 'Profil',
                          ket:
                              'Data yang harus anda lengkapi untuk memulai monitoring',
                        ),
                        list.length == 0
                            ? CardHome(
                                tap: () {
                                  showTopSnackBar(
                                    context,
                                    CustomSnackBar.error(
                                      message: 'Isi Terlebih Dahulu Profil!',
                                    ),
                                  );
                                },
                                judul: 'Data Monitoring',
                                ket:
                                    'Berisikan data  - data  monitoring yang telah anda buat selama anda bimbingan Tugas Akhir',
                              )
                            : CardHome(
                                tap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DataMonitoringMhs(
                                                nim: widget.nim,
                                                name: widget.name,
                                                idProfil:
                                                    list[0].idProfil.toString(),
                                              )));
                                },
                                judul: 'Data Monitoring',
                                ket:
                                    'Berisikan data  - data  monitoring yang telah anda buat selama anda bimbingan Tugas Akhir',
                              ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
