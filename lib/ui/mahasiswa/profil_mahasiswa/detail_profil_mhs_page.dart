import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_detail_profil.dart';
import 'package:monitoring_skripsi/repository/repo_profil.dart';
import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/formDetailData.dart';

class DetailProfilPage extends StatefulWidget {
  final String nim;
  final String nama;
  const DetailProfilPage({Key key, this.nim, this.nama}) : super(key: key);

  @override
  _DetailProfilPageState createState() => _DetailProfilPageState();
}

class _DetailProfilPageState extends State<DetailProfilPage> {
  List<MdlDetailProfil> list = [];
  Future<List<MdlDetailProfil>> fetch() async {
    var repo = await RepoProfil().fetchDetail(widget.nim);
    setState(() {
      list = repo;
    });
    return repo;
  }

  @override
  void initState() {
    fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            CostumeAppBar(
              size: null,
              judul: ' Detail Profil',
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: 20, bottom: 10, left: 30, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FormDetailData(
                    judul: 'Dosen Pembimbing 1  : ',
                    // ignore: null_aware_before_operator
                    data: list?.length > 0 ? list[0].dosenPembimbing1 : '',
                  ),
                  FormDetailData(
                    judul: 'Dosen Pembimbing 2  : ',
                    data: list?.length > 0 ? list[0].dosenPembimbing2 : '',
                  ),
                  FormDetailData(
                    judul: 'Judul Skripsi                 : \n',
                    data: list?.length > 0 ? list[0].judul : '',
                  ),
                  FormDetailData(
                    judul: 'Tanggal Mulai               : ',
                    data: list?.length > 0 ? list[0].tanggalMulai : '',
                  ),
                  FormDetailData(
                    judul: 'Tanggal Selesai            : ',
                    data: list?.length > 0 ? list[0].tanggalSelesai : '',
                  ),
                  FormDetailData(
                    judul: 'Status                             : ',
                    data: list?.length > 0 ? list[0].statusBimbingan : '',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
