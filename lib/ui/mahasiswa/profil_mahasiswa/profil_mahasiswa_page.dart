import 'dart:async';
import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/model/mdl_dosen.dart';
import 'package:monitoring_skripsi/repository/repo_profil.dart';
import 'package:monitoring_skripsi/service/admin_service.dart';

import 'package:monitoring_skripsi/ui/widgets/costume_appbar.dart';
import 'package:monitoring_skripsi/ui/widgets/datepicker.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../constant.dart';
import '../home_page_mahasiswa.dart';

class Profil extends StatefulWidget {
  final String nim;
  final String nama;
  final judul = TextEditingController();
  final tanggal1 = TextEditingController();
  final tanggal2 = TextEditingController();

  Profil({
    Key key,
    this.nim,
    this.nama,
  }) : super(key: key);
  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  List<MdlDosen> listDosen;
  Timer timer;
  String dosbim1;
  String dosbim2;
  String status;

  Future fetchDosen() async {
    var dataDosen = await AdminService().fetchDosen();
    setState(() {
      listDosen = dataDosen;
    });
  }

  @override
  void initState() {
    fetchDosen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              CostumeAppBar(
                size: size,
                judul: 'Profil',
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      LabelForm(
                        label: 'Judul Skripsi',
                      ),
                      TextForm(
                        icon: Icon(Icons.book, color: hijau),
                        benar: false,
                        cr: widget.judul,
                        hint: 'Masukan Judul Skripsi',
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      LabelForm(
                        label: 'Dosen Pembimbing 1',
                      ),
                      Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset:
                                    Offset(0, 5), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.circular(30)),
                        child: DropdownButton<String>(
                          hint: Container(
                            width: size.width / 2,
                            child: Text(
                              "Pilih Dosen Pembimbing 1",
                              style: TextStyle(color: Colors.grey),
                              textAlign: TextAlign.end,
                            ),
                          ),
                          value: dosbim1,
                          icon: Padding(
                            padding: EdgeInsets.only(left: size.width / 4),
                            child: Icon(
                              Icons.arrow_drop_down,
                              color: hijau,
                            ),
                          ),
                          iconSize: 40,
                          underline: SizedBox(),
                          onChanged: (String newValue) {
                            setState(() {
                              dosbim1 = newValue;
                            });
                          },
                          items: listDosen?.map((list) {
                                return DropdownMenuItem(
                                  child: Text(list.nama),
                                  value: list.nidn,
                                );
                              })?.toList() ??
                              [],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      LabelForm(
                        label: 'Dosen Pembimbing 2',
                      ),
                      Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset:
                                    Offset(0, 5), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.circular(30)),
                        child: DropdownButton<String>(
                          hint: Container(
                            width: size.width / 2,
                            child: Text(
                              "Pilih Dosen Pembimbing 2",
                              style: TextStyle(color: Colors.grey),
                              textAlign: TextAlign.end,
                            ),
                          ),
                          value: dosbim2,
                          icon: Padding(
                            padding: EdgeInsets.only(left: size.width / 4),
                            child: Icon(
                              Icons.arrow_drop_down,
                              color: hijau,
                            ),
                          ),
                          iconSize: 40,
                          underline: SizedBox(),
                          onChanged: (String newValue) {
                            setState(() {
                              dosbim2 = newValue;
                            });
                          },
                          items: listDosen?.map((list) {
                                return DropdownMenuItem(
                                  child: Text(list.nama),
                                  value: list.nidn,
                                );
                              })?.toList() ??
                              [],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      LabelForm(
                        label: 'Tanggal Aktivasi',
                      ),
                      DatePickerWidget(
                        ctr: widget.tanggal1,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      LabelForm(
                        label: 'Tanggal Selesai',
                      ),
                      DatePickerWidget(
                        ctr: widget.tanggal2,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      LabelForm(
                        label: 'Status',
                      ),
                      Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset:
                                    Offset(0, 5), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.circular(30)),
                        child: DropdownButton<String>(
                            hint: Container(
                              width: size.width / 8,
                              child: Text(
                                "Status",
                                style: TextStyle(color: Colors.grey),
                                textAlign: TextAlign.end,
                              ),
                            ),
                            value: status,
                            icon: Padding(
                              padding: const EdgeInsets.only(left: 200),
                              child: Icon(
                                Icons.arrow_drop_down,
                                color: hijau,
                              ),
                            ),
                            iconSize: 40,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                status = newValue;
                              });
                            },
                            items: <String>[
                              'Aktif',
                              'Diperpanjang',
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    value,
                                    style: TextStyle(color: hijau),
                                  ),
                                ),
                              );
                            }).toList()),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      DefaultButton(
                        ket: 'Submit',
                        ontap: () {
                          add();
                        },
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void add() {
    if (dosbim1 == dosbim2) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: 'Dosen Pembimbing Tidak Boleh Sama',
        ),
      );
    } else {
      RepoProfil().addProfil(
          widget.nim,
          widget.judul.text,
          widget.tanggal1.text,
          widget.tanggal2.text,
          dosbim1,
          dosbim2,
          '1',
          '20');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(
                    name: widget.nama,
                    nim: widget.nim,
                  )));
      showTopSnackBar(
        context,
        CustomSnackBar.success(
          message: 'Berhasil',
        ),
      );
    }
  }
}
