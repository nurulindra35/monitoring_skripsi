import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/repository/auth_repo.dart';
import 'package:monitoring_skripsi/service/validation.dart';
import 'package:monitoring_skripsi/ui/login_page/login_page.dart';
import 'package:monitoring_skripsi/ui/widgets/default_button.dart';
import 'package:monitoring_skripsi/ui/widgets/keterangan_form.dart';
import 'package:monitoring_skripsi/ui/widgets/label_home.dart';
import 'package:monitoring_skripsi/ui/widgets/textform.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../constant.dart';

class BodyRegister extends StatefulWidget {
  @override
  _BodyRegisterState createState() => _BodyRegisterState();
}

class _BodyRegisterState extends State<BodyRegister> with Validation {
  final formKey = GlobalKey<FormState>();
  final nama = TextEditingController();

  final nim = TextEditingController();

  final email = TextEditingController();

  final password = TextEditingController();

  final nohp = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
      child: Form(
        key: formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LabelForm(
              label: 'Nama Lengkap',
            ),
            TextForm(
              val: validateName,
              icon: Icon(
                Icons.person,
                color: hijau,
              ),
              benar: false,
              hint: 'Masukan Nama Lengkap',
              cr: nama,
            ),
            SizedBox(
              height: 20,
            ),
            LabelForm(
              label: 'NIM',
            ),
            TextFormDigit(
              val: validateNIM,
              icon: Icon(
                Icons.confirmation_num,
                color: hijau,
              ),
              benar: false,
              cr: nim,
              hint: 'Masukan NIM',
              max: 11,
            ),
            SizedBox(
              height: 20,
            ),
            LabelForm(
              label: 'E-mail',
            ),
            TextForm(
              val: validateEmail,
              icon: Icon(
                Icons.mail,
                color: hijau,
              ),
              benar: false,
              cr: email,
              hint: 'Masukan E-Mail',
            ),
            SizedBox(
              height: 20,
            ),
            LabelForm(
              label: 'Password',
            ),
            TextFormPass(
              val: validatePassword,
              icon: Icon(
                Icons.lock,
                color: hijau,
              ),
              benar: true,
              cr: password,
              hint: 'Masukan Password',
            ),
            SizedBox(
              height: 20,
            ),
            LabelForm(
              label: 'No HP',
            ),
            TextFormDigit(
              val: validateNoHp,
              icon: Icon(Icons.phone, color: hijau),
              benar: false,
              max: 13,
              cr: nohp,
              hint: 'Masukan No HP',
            ),
            SizedBox(
              height: 20,
            ),
            DefaultButton(
              ontap: () {
                if (formKey.currentState.validate()) {
                  register();
                }
              },
              ket: 'Register',
            ),
            KeteranganForm(
              ket1: 'Sudah punya akun? ',
              ket2: 'Login Disini',
              ontap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  register() async {
    var regis = await AuthRepo().repoRegister(
        nim.text, nama.text, email.text, password.text, nohp.text);

    if (regis['nim'] == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: regis['message'],
        ),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Register Berhasil',
        ),
      );
    }
  }
}
