import 'package:flutter/material.dart';
import 'package:monitoring_skripsi/ui/register_page/body_register/body_register_page.dart';
import 'package:monitoring_skripsi/ui/widgets/header.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderAuth(
                auth: 'Register',
              ),
              BodyRegister()
            ],
          ),
        ),
      ),
    );
  }
}
