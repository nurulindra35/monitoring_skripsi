import 'package:flutter/material.dart';

import '../../constant.dart';

class CostumeAppBar extends StatelessWidget {
  final String judul;
  final Icon icons;
  final Function tf;

  CostumeAppBar({
    Key key,
    @required this.size,
    this.judul,
    this.tf,
    this.icons,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: hijau,
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(
                  flex: 3,
                ),
                Text(
                  judul,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(
                  flex: 4,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CostumeAppBar2 extends StatelessWidget {
  final String judul;
  final Icon icons;
  final Function tf;

  CostumeAppBar2({
    Key key,
    @required this.size,
    this.judul,
    this.tf,
    this.icons,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          IconButton(
            onPressed: tf,
            icon: Icon(
              Icons.arrow_back_ios,
              color: hijau,
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(
                  flex: 3,
                ),
                Text(
                  judul,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(
                  flex: 4,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
