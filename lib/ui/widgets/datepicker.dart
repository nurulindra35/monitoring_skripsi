import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';

import '../../constant.dart';


// ignore: must_be_immutable
class DatePickerWidget extends StatefulWidget {
  final TextEditingController ctr;
  final Function bloc;

  const DatePickerWidget({
    Key key,
    this.ctr,
    this.bloc,
  }) : super(key: key);
  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.transparent,
        child: Center(
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 5), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.circular(30)),
            child: Padding(
              padding: const EdgeInsets.all(2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.event,
                    color: hijau,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: DateTimePicker(
                      onChanged: widget.bloc,
                      type: DateTimePickerType.date,
                      dateMask: 'd MMMM yyyy',
                      controller: widget.ctr,
                      //initialValue: _initialValue,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100),
                      dateLabelText: 'Date',
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        color: hijau,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
