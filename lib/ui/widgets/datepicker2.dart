import 'package:flutter/material.dart';

import '../../constant.dart';

// ignore: must_be_immutable
class DataPicker2 extends StatefulWidget {
   DataPicker2({Key key, this.title, this.selectedDate}) : super(key: key);

  @override
  _DataPicker2State createState() => _DataPicker2State();
  final String title;
  DateTime selectedDate = DateTime.now();
}

class _DataPicker2State extends State<DataPicker2> {
 

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: widget.selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != widget.selectedDate)
      setState(() {
        widget.selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.circular(30)),
        child: Padding(
          padding: const EdgeInsets.all(2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(Icons.date_range, color: hijau),
              SizedBox(
                width: 10,
              ),
              Text(
                "${widget.selectedDate.toLocal()}".split(' ')[0],
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
              ),
              Expanded(
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () => _selectDate(context),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Select date',
                            style: TextStyle(backgroundColor: Colors.transparent),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
