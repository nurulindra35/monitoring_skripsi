import 'package:flutter/material.dart';

import '../../constant.dart';

class DefaultButton extends StatelessWidget {
  final String ket;
  final Function ontap;
  DefaultButton({
    Key key,
    this.ket,
    this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(30), color: hijau),
      child: Material(
        borderRadius: BorderRadius.circular(30),
        shadowColor: Colors.black45,
        color: Colors.transparent,
        elevation: 7.0,
        child: InkWell(
          onTap: ontap,
          child: Center(
            child: Text(
              ket,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}

class DefaultButton2 extends StatelessWidget {
  final String ket;
  final Function ontap;
  DefaultButton2({
    Key key,
    this.ket,
    this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30), color: Colors.red),
      child: Material(
        borderRadius: BorderRadius.circular(30),
        shadowColor: Colors.black45,
        color: Colors.transparent,
        elevation: 7.0,
        child: InkWell(
          onTap: ontap,
          child: Center(
            child: Text(
              ket,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}
