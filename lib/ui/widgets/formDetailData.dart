import 'package:flutter/material.dart';

class FormDetailData extends StatelessWidget {
  final String judul;
  final String data;
  const FormDetailData({
    Key key,
    this.judul,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            judul,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Flexible(
            child: Text(
              data,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }
}
