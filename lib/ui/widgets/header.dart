import 'package:flutter/material.dart';

import '../../constant.dart';

class HeaderAuth extends StatelessWidget {
  final String auth;
  const HeaderAuth({
    Key key,
    this.auth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          'images/header.png',
          width: double.infinity,
          fit: BoxFit.fitWidth,
        ),
        Padding(
          padding: EdgeInsets.only(right: 20, top: 40, left: 20, bottom: 30),
          child: Row(
            children: [
              Image.asset('images/ti_logo.png'),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Selamat Datang Di Monitoring\n',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    TextSpan(
                      text: 'Tugas Akhir Prodi TI UMY',
                      style: TextStyle(
                          fontSize: 20,
                          color: hijau,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: '$auth\n',
                  style: TextStyle(
                      fontSize: 32,
                      color: Color(0xff0DB267),
                      fontWeight: FontWeight.bold),
                ),
                TextSpan(
                  text: 'Untuk Memulai',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold),
                ),
                TextSpan(
                  text: ' Monitoring',
                  style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff0DB267),
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
