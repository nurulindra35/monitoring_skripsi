import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../constant.dart';

class KeteranganForm extends StatelessWidget {
  final String ket1;
  final String ket2;
  final Function ontap;
  const KeteranganForm({
    Key key, this.ket1, this.ket2, this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, top: 10,bottom: 10),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: ket1,
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
              ),
            ),
            TextSpan(
              recognizer: TapGestureRecognizer()
                ..onTap = ontap,
              text: ket2,
              style: TextStyle(
                  fontSize: 20, color: hijau, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
