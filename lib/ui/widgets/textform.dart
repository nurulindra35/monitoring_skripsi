import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../constant.dart';

class TextForm extends StatefulWidget {
  final String hint;
  final Function tf;
  final TextEditingController cr;
  final bool benar;
  final Icon icon;
  final int length;
  final Function val;
  TextForm({
    Key key,
    this.hint,
    this.cr,
    this.benar,
    this.icon,
    this.tf,
    this.length,
    this.val,
  }) : super(key: key);

  @override
  _TextFormState createState() => _TextFormState();
}

class _TextFormState extends State<TextForm> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Material(
        color: Colors.white,
        elevation: 5,
        shadowColor: Colors.black,
        borderRadius: BorderRadius.circular(30),
        child: TextFormField(
          validator: widget.val,
          controller: widget.cr,
          onChanged: widget.tf,
          maxLength: widget.length,
          obscureText: widget.benar,
          style: TextStyle(color: hijau),
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(
              errorStyle: TextStyle(
                fontSize: 12.0,
                backgroundColor: Colors.transparent,
              ),
              counterStyle: TextStyle(fontSize: 10),
              prefixIcon: widget.icon,
              fillColor: Colors.white,
              filled: true,
              hintText: widget.hint,
              floatingLabelBehavior: FloatingLabelBehavior.auto,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(color: Colors.white),
                  gapPadding: 10),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(color: Colors.white),
                  gapPadding: 10)),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class TextFormPass extends StatefulWidget {
  final String hint;
  final TextEditingController cr;
  final Function val;
  bool benar = true;
  final Icon icon;
  final Function tf;
  TextFormPass({
    Key key,
    this.hint,
    this.cr,
    this.benar,
    this.icon,
    this.tf,
    this.val,
  }) : super(key: key);

  @override
  _TextFormPassState createState() => _TextFormPassState();
}

class _TextFormPassState extends State<TextFormPass> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      shadowColor: Colors.black,
      borderRadius: BorderRadius.circular(30),
      child: TextFormField(
        onChanged: widget.tf,
        validator: widget.val,
        style: TextStyle(color: hijau),
        controller: widget.cr,
        obscureText: widget.benar,
        decoration: InputDecoration(
          prefixIcon: widget.icon,
          fillColor: Colors.white,
          filled: true,
          hintText: widget.hint,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(color: Colors.white),
              gapPadding: 10),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(color: Colors.white),
              gapPadding: 10),
          suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
              widget.benar ? Icons.visibility : Icons.visibility_off,
              color: hijau,
            ),
            onPressed: () {
              // Update the state i.e. toogle the state of passwordVisible variable
              setState(() {
                widget.benar = !widget.benar;
              });
            },
          ),
        ),
      ),
    );
  }
}

class TextFormDigit extends StatefulWidget {
  final String hint;
  final TextEditingController cr;
  final bool benar;
  final Icon icon;
  final Function tf;
  final int max;
  final Function val;
  TextFormDigit({
    Key key,
    this.hint,
    this.cr,
    this.benar,
    this.icon,
    this.tf,
    this.max,
    this.val,
  }) : super(key: key);

  @override
  _TextFormDigit createState() => _TextFormDigit();
}

class _TextFormDigit extends State<TextFormDigit> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      shadowColor: Colors.black,
      borderRadius: BorderRadius.circular(30),
      child: TextFormField(
        style: TextStyle(color: hijau),
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
        onChanged: widget.tf,
        validator: widget.val,
        maxLength: widget.max,
        controller: widget.cr,
        obscureText: widget.benar,
        decoration: InputDecoration(
          counterText: '',
          counterStyle: TextStyle(fontSize: 0),
          prefixIcon: widget.icon,
          fillColor: Colors.white,
          filled: true,
          hintText: widget.hint,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(color: Colors.white),
              gapPadding: 10),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(color: Colors.white),
              gapPadding: 10),
        ),
      ),
    );
  }
}
